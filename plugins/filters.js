import Vue from 'vue'

export const slugify = function(value) {
  value = value.replace(/^\s+|\s+$/g, '') // trim
  value = value.toLowerCase()

  // remove accents, swap ñ for n, etc
  var from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;'
  var to = 'aaaaaeeeeeiiiiooooouuuunc------'
  for (var i = 0, l = from.length; i < l; i++) {
    value = value.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
  }

  value = value
    .replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-') // collapse dashes

  return value
}
export const moneyFormatter = function(value, locale, currency, symbol) {
  if (!locale) {
    value = Intl.NumberFormat('en-us', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 0
    })
      .format(value)
      .replace('$', 'US$ ')
  } else {
    value = Intl.NumberFormat(locale, {
      style: 'currency',
      currency: currency,
      minimumFractionDigits: 0
    })
      .format(value)
      .replace(symbol, symbol + ' ')
  }
  return value
}

export const isPrecioLista = function(value, type, index) {
  if (type == 'Precios' && index != 0) {
    if (value) {
      return (value = Intl.NumberFormat('en-us', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 0
      })
        .format(value)
        .replace('$', 'US$ '))
    } else {
      return ''
    }
  }
  {
    return value
  }
}

const stripTags = value => {
  return value.replace(/<.+?>/g, '')
}

Vue.filter('slugify', slugify)
Vue.filter('stripTags', stripTags)

Vue.filter('isPrecioLista', isPrecioLista)
Vue.filter('moneyFormatter', moneyFormatter)
