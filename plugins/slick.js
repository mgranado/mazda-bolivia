import Slick from 'vue-slick';
import Vue from 'vue';

const SlickCarousel = {
  install(Vue, options) {
    Vue.component('slick', Slick)
  }
};
Vue.use(SlickCarousel);

export default SlickCarousel;