export default function ({ $axios, redirect }) {
  $axios.onRequest(config => {
    config.headers.common['blog-url'] = process.env.baseUrl;
  })
}