import Vue from 'vue'
import { VueHammer } from 'vue2-hammer'

// // change the threshold for all pan recognizers
// VueHammer.config.pan = {
//   threshold: 200
// };

Vue.use(VueHammer)
