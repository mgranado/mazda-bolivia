export default {
  methods: {
    //masks
    onlyNumbersMask(evt) {
      var charCode = evt.which ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return evt.preventDefault()
    },
    onlyAlphaMask(evt) {
      var charCode = evt.which ? evt.which : event.keyCode
      if (
        charCode > 31 &&
        (charCode < 65 || charCode > 90) &&
        (charCode < 97 || charCode > 122)
      )
        return evt.preventDefault()
    },
    maskRut(elementId) {
      let vm = this
        ; (function (a) {
          a.fn.validCampoFranz = function (b) {
            a(this).on({
              keypress: function (a) {
                var c = a.which,
                  d = a.keyCode,
                  e = String.fromCharCode(c).toLowerCase(),
                  f = b
                  ; ((-1 != f.indexOf(e) ||
                    9 == d ||
                    (37 != c && 37 == d) ||
                    (39 == d && 39 != c) ||
                    8 == d ||
                    (46 == d && 46 != c)) &&
                    161 != c) ||
                    a.preventDefault()
              }
            })
          }
        })($)
      $(elementId).validCampoFranz('1234567890-kK')
      $(elementId).focusout(function () {
        var rut = $(this).val()
        rut = rut.replace(/-/g, '')
        var sRut1 = rut
        var nPos = 0
        var sInvertido = ''
        var sRut = ''
        for (var i = sRut1.length - 1; i >= 0; i--) {
          sInvertido += sRut1.charAt(i)
          if (i == sRut1.length - 1) sInvertido += '-'
          else if (nPos == 3) {
            sInvertido += ''
            nPos = 0
          }
          nPos++
        }
        for (var j = sInvertido.length - 1; j >= 0; j--) {
          if (sInvertido.charAt(sInvertido.length - 1) != '.')
            sRut += sInvertido.charAt(j)
          else if (j != sInvertido.length - 1) sRut += sInvertido.charAt(j)
        }
        vm.rut = sRut
        $(this).val(sRut.toUpperCase())
      })
    },
    validateRut(rut) {
      // Despejar Puntos
      let valor = rut.replace(/\./g, '');
      // Despejar Guión
      valor = valor.replace('-', '');

      // Aislar Cuerpo y Dígito Verificador
      let cuerpo = valor.slice(0, -1);
      let dv = valor.slice(-1).toUpperCase();

      // Formatear RUN
      rut = cuerpo + '-' + dv

      // Si no cumple con el mínimo ej. (n.nnn.nnn)
      if (cuerpo.length < 7) { return false; }

      // Calcular Dígito Verificador
      let suma = 0;
      let multiplo = 2;

      // Para cada dígito del Cuerpo
      for (let i = 1; i <= cuerpo.length; i++) {

        // Obtener su Producto con el Múltiplo Correspondiente
        let index = multiplo * valor.charAt(cuerpo.length - i);

        // Sumar al Contador General
        suma = suma + index;

        // Consolidar Múltiplo dentro del rango [2,7]
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

      }

      // Calcular Dígito Verificador en base al Módulo 11
      let dvEsperado = 11 - (suma % 11);

      // Casos Especiales (0 y K)
      dv = (dv == 'K') ? 10 : dv;
      dv = (dv == 0) ? 11 : dv;

      // Validar que el Cuerpo coincide con su Dígito Verificador
      if (dvEsperado != dv) { return false; }

      // Si todo sale bien, eliminar errores (decretar que es válido)
      return true
    },
    //validations
    isValidInput(inputName) {

      const vm = this
      let obj = {
        'last_name': vm.isValidLastName(this.last_name),
        'first_name': vm.isValidFirstName(this.first_name),
        'email': vm.isValidEmail(this.email),
        'phone': vm.isValidPhone(this.phone),
        'rut': vm.isValidRut(this.rut),
        'subsidiary': vm.isValidSubsidiary(this.subsidiary),
      }
      if (!obj[inputName] && !this.errors.find(e => e === inputName)) {
        this.errors.push(inputName)
      }
      if (obj[inputName] && this.errors.find(e => e === inputName)) {
        this.errors.splice(this.errors.indexOf(inputName), 1)
      }
      console.log(this.errors);

    },
    isValidEmail(email) {
      'use strict'
      var pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      )
      return pattern.test(email)
    },
    isValidPhone(phone) {
      return phone.length >= 9
    },
    isValidFirstName(str) {
      return str.length >= 2
    },
    isValidLastName(str) {
      return str.length >= 2
    },
    isValidRut(data) {
      if (!data) return false
      return this.validateRut(data)
    },
    isValidSubsidiary(data) {
      return data
    },
    isValidDocumentType(data) {
      return data
    },
    validateInputForms() {
      this.errors = []
      if (!this.isValidEmail(this.email)) {
        this.errors.push('email')
      }
      if (!this.isValidPhone(this.phone)) {
        this.errors.push('phone')
      }
      if (!this.isValidFirstName(this.first_name)) {
        this.errors.push('first_name')
      }
      if (!this.isValidLastName(this.last_name)) {
        this.errors.push('last_name')
      }
      if (!this.isValidRut(this.rut)) {
        this.errors.push('rut')
      }
      if (!this.subsidiary) {
        this.errors.push('subsidiary')
      }
      if (!this.errors.length) {
        return true
      }
    }
  }
}
