import pkg from './package'
require('dotenv').config()
export default {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://cdn.jsdelivr.net/npm/foundation-sites@6.5.3/dist/css/foundation.min.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.8.1/css/all.css'
      }
    ],
    script: [
      {
        src: 'https://code.jquery.com/jquery-3.4.1.min.js'
      },
      {
        src:
          'https://cdn.jsdelivr.net/npm/foundation-sites@6.5.3/dist/js/foundation.min.js'
      }
    ]
  },

  /*
   ** Customize the environment variables
   */
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000',
    apiBaseUrl: process.env.API_BASE_URL || 'http://localhost:5000',
    captchaSiteKey: process.env.CAPTCHA_SITE_KEY,
    minPrice: process.env.MIN_PRICE,
    maxPrice: process.env.MAX_PRICE,
    remoteAssetsUrl:
      process.env.REMOTE_ACCESS_URL ||
      'https://mazda-website-solution.s1.umbraco.io'
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: ['@/assets/scss/main.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/hammer', ssr: false },
    { src: '~/plugins/slick', ssr: false },
    { src: '~/plugins/smooth-scroll', ssr: false },
    { src: '~/plugins/affix', ssr: false },
    '~/plugins/filters',
    '~/plugins/axios'
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    'vue-scrollto/nuxt',
    '@nuxtjs/recaptcha'
  ],
  recaptcha: {
    hideBadge: false, // Hide badge element (v3)
    siteKey: process.env.CAPTCHA_SITE_KEY,    // Site key for requests
    version: 3,    // Version
  },
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.API_BASE_URL
  },

  server: {
    port: process.env.PORT || 3000 // default: 3000
  },

  /*
   ** Build configuration
   */
  build: {
    vendor: ['vue-slick'],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) { }
  }
}
