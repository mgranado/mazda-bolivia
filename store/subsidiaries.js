
export const state = () => ({
  filters: {},
  zones: []
})

export const getters = {
  getSubsidiaryByCode: (state) => (code) => {
    let subsidiaries = state.zones.map(z => z.subsidiaries).reduce((acc, val) => acc.concat(val), [])
    return subsidiaries.find(s => s.code === code)
  },

  filterByZone: (state) => (slugZone) => {
    return state.zones.filter(zone => zone.slug === slugZone);
  },

  filterByMunicipality: (state, getters) => (slugParams) => {
    let zone = getters['filterByZone'](slugParams.zone);
    if (zone.length) {
      return zone.map(z => {
        let subsidiariesFiltered = z.subsidiaries.filter(subsidiary => subsidiary.commune.slug === slugParams.municipality);
        z = { ...z, subsidiaries: subsidiariesFiltered };
        return z;
      });
    } else {
      return [];
    }
  },
  filterByService: (state, getters) => (slugParams) => {

    if (slugParams.zone !== 'todos') {

      if (slugParams.municipality !== 'todos') {

        // Slug /{zona}/{municipalidad}/{servicio}
        let zonesFiltered = getters['filterByMunicipality'](slugParams);
        if (zonesFiltered.length) {
          let subsidiariesToShow = zonesFiltered[0].subsidiaries.filter(subsidiary => {
            let services = subsidiary.services.filter(service => service.slug === slugParams.service)
            return services.length > 0;
          });
          // zonesFiltered[0].subsidiaries = subsidiariesToShow;
          zonesFiltered[0] = { ...zonesFiltered[0], subsidiaries: subsidiariesToShow }
          return zonesFiltered;
        } else {
          return []
        }

      } else {

        // Slug /{zona}/todos/{servicio}
        let zonesFiltered = getters['filterByZone'](slugParams.zone);
        if (zonesFiltered.length) {
          let subsidiariesToShow = zonesFiltered[0].subsidiaries.filter(subsidiary => {
            let services = subsidiary.services.filter(service => service.slug === slugParams.service)
            return services.length > 0;
          });
          // zonesFiltered[0].subsidiaries = subsidiariesToShow;
          zonesFiltered[0] = { ...zonesFiltered[0], subsidiaries: subsidiariesToShow }
          return zonesFiltered;
        } else {
          return []
        }

      }

    } else {

      if (slugParams.municipality === 'todos') {

        // Slug /todos/todos/{servicio}
        let allZones = state.zones;
        let desiredSubsidiaries = [];
        allZones.forEach(zone => {
          let desiredSubsidiariesByZone = zone.subsidiaries.filter(subsidiary => {
            let services = subsidiary.services.filter(service => service.slug === slugParams.service);
            return services.length > 0;
          });
          desiredSubsidiariesByZone.forEach(desiredSubsidiaryByZone => {
            desiredSubsidiaries.push(desiredSubsidiaryByZone);
          })
        });

        return [
          {
            name: slugParams.service.replace(/-/g, ' '),
            subsidiaries: desiredSubsidiaries
          }
        ]

      } else {

        return []

      }

    }

  }
}

export const mutations = {
  setFilters(state, filters) {
    state.filters = filters;
  },
  setZones(state, zones) {
    state.zones = zones;
  }
}

export const actions = {
  setFilters({ commit }) {
    this.$axios.get('subsidiaries/filters')
      .then(res => {
        commit('setFilters', res.data.filters);
      })
  },
  setZones({ commit }, zones) {
    commit('setZones', zones);
    // this.$axios.get('subsidiaries')
    //   .then(res => {
    //     commit('setZones', res.data.regions);
    //   })
  }
};
