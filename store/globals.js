export const state = () => ({
  globals: {
    title: 'Suzuki Perú - Way of Life',
    country: 'Chile',
    header: {
      logo:
        'https://mazda-website-solution.s1.umbraco.io/media/3504/mazda_logo_global_nav3.png',
      main_menu: [
        {
          text: 'MODELOS',
          url: '#',
          target: '_parent'
        },
        {
          text: 'POR QUÉ MAZDA',
          url: '#',
          target: '_parent'
        }
      ],
      security_alert: {
        text: 'Llamados a Revisión',
        url: 'https://derco.com.pe/campanadeseguridad/',
        target: '_blank'
      },
      social_menu: [
        {
          iconClass: 'icon-Facebook',
          url: 'https://www.facebook.com/DercoPeru/',
          social_network: 'Facebook',
          target: '_blank'
        },
        {
          iconClass: 'icon-Twitter',
          url: 'https://twitter.com/derco_peru',
          social_network: 'Twitter',
          target: '_blank'
        },
        {
          iconClass: 'icon-icon-youtube-lleno',
          url: 'https://www.youtube.com/channel/UCHlQLjbcPFZk38sWRl8AFRA',
          social_network: 'Youtube',
          target: '_blank'
        }
      ],
      topMenu: [
        {
          text: 'Concesionarios',
          url: '/concesionarios',
          target: '_parent'
        },
        {
          text: 'contacto',
          url: '/contacto',
          target: '_parent'
        },
        {
          text: 'alerta de seguridad',
          url: 'https://www.dercocenter.cl/recall/',
          target: '_blank'
        }
      ]
    },
    terms: {
      text: `Legal sobre las características de los vehículos: Las características y dimensiones dependen de cada modelo de vehículo. Las características de los vehículos pueden estar sujetas a variaciones sin previo aviso debido a instrucciones del fabricante, cambio de modelos y/o acciones preventivas.</br>
      Legal sobre fotografías: Las imágenes de los vehículos se exhiben con fines referenciales y son meramente ilustrativas. Podrían existir diferencias entre las imágenes y las dimensiones y características reales de los mismos. La información referida a las dimensiones y características reales de los vehículos se encuentra en las correspondientes fichas técnicas de cada uno de ellos.</br>
      Legal de cotizadores:Tipo de cambio referencial: S/ 3.5, sujeto a variaciones y fluctuaciones del mercado. Consulte el tipo de cambio actualizado en DERCOCENTER S.A.C. o en la Red de concesionarios DERCOCENTER. Precios válidos del día 01 al 31 en el mes correspondiente de la promoción y/o hasta acabar stock (stock mínimo de unidades: 5). Se recargará un 2% sobre el precio del vehículo si se utiliza como medio de pago una tarjeta de crédito o débito.</br>
      `
    },
    footer: [
      {
        tools: [
          {
            value: 'Selección de precios'
          },
          {
            value: 'Busqueda de inventario'
          },
          {
            value: 'Pedir presupuesto'
          }
        ]
      },
      {
        otrosSitios: [
          {
            value: 'Mazda Global'
          },
          {
            value: 'Fundación  Mazda'
          }
        ]
      },
      {
        about: [
          {
            value: 'Auto Show'
          },
          {
            value: 'Mazda New'
          }
        ]
      },
      {
        help: [
          {
            value: 'Sistemap'
          },
          {
            value: 'Preguntas Frecuentes'
          }
        ]
      }
    ]
  }
})

export const getters = {
  getAllGlobals: state => {
    return state.globals
  },
  getTerms: state => {
    return state.globals.terms.text
  }
}

export const mutations = {
  set_all_globals: (state, globals) => {
    state.globals = globals
  }
}
