import { slugify } from '@/plugins/filters'

export const state = () => ({
  list: [],
  currentModel: []
})

export const mutations = {
  setModels(state, models) {
    state.list = models
  },
  setCurrentModel(state, model) {
    state.currentModel = model
  }
}

export const actions = {
  setModels({ commit, state }) {
    return new Promise(resolve => {
      this.$axios.get('models').then(response => {
        commit('setModels', response.data)
        resolve()
      })
    })
  }
}

export const getters = {
  getAll: (state) => {
    return state.list
  },
  getCarClasses: (state) => {
    return state.list
      .map(m => m.carClass)
      .reduce((acc, val) => acc.concat(val), [])
      .map(t => slugify(t.toLowerCase()))
      .filter((v, i, a) => a.indexOf(v) === i)
  },
  modelsWithCleanCarClasses: state => {
    return state.list.map(m => {
      // console.log(m.carClass.map(t => slugify(t.toLowerCase().trim())))
      return Object.assign({}, m, {
        carClass: m.carClass.map(t => slugify(t.toLowerCase().trim()))
      })
    })
  },
  getCurrentModel: state => {
    return state.currentModel
  },
  getModelsByType: (state, getters) => (type) => {
    return getters.modelsWithCleanCarClasses.filter(m =>
      m.carClass.includes(type)
    )
  }
}
