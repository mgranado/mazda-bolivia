import { slugify } from '~/plugins/filters'

export const state = () => ({
  list: [],
  version: null
})

export const getters = {
  getAll: (state) => {
    return state.list
  },
  sorterByPrice: (state) => (order) => {
    return state.list.sort(function (a, b) {
      return order === 'asc' ? a['minPrice'] - b['minPrice'] : b['minPrice'] - a['minPrice']
    })
  },
  filterByModels: (state) => (models, versions) => {
    return versions.filter(v => {
      return models.includes(v.model.slug)
    })
  },
  getVersionById: (state) => (ids) => {
    return state.list.filter(v => ids.includes(v.id))
  },
  getVersion: (state) => {
    return state.version
  },
  filterByType: (state) => (type, versions) => {
    let cleanVersions = versions.map(v => {
      var obj = { ...v, model: { ...v.model, carClass: v.model.carClass.map(t => slugify(t)) } }
      return obj
    })
    return cleanVersions.filter(v => {
      return v.model.carClass.indexOf(type) > -1
    })
  }
}

export const mutations = {
  set_versions(state, versions) {
    state.list = versions;
  },
  set_version(state, version) {
    state.version = version;
  }
}
export const actions = {
  setVersions({ commit, state }) {
    return new Promise(resolve => {
      this.$axios.get('versions').then(response => {
        commit('set_versions', response.data)
        resolve()
      })
    })
  },
  fetchVersionbyId({ commit }, id) {
    return new Promise(resolve => {
      this.$axios.get('versions/' + id).then(response => {
        commit('set_version', response.data[0])
        resolve()
      })
    })
  }
}
